import React from "react";
import "./header.scss";

class Header extends React.Component {
  render() {
    return (
        <div className="header">
          {/* <span>Header</span> */}
          {this.props.children}
        </div>
    );
  }
}
export default Header;
