import React from "react";
import "./SignInSignUpPattern.scss";
import LogIn from "../LogIn/LogIn";
import SignUp from "../SignUp/SignUp";
import RestorePassword from "../RestorePassword/RestorePassword";

class SignInSignUpPattern extends React.Component<any, any> {
  state = {
    switchPage: this.props.startPage,
  };

  switchComponent() {
    switch (this.state.switchPage) {
      case "LogIn":
        return (
          <LogIn
            close={this.props.close}
            onPressCustom={() => this.setState({ switchPage: "RestorePassword" })}
            onPressCustom2={() => this.setState({ switchPage: "SignUp" })}
          />
        );
      case "RestorePassword":
        return (
          <RestorePassword onPressCustom={() => this.setState({ switchPage: "LogIn" })} /> 
        );
      case "SignUp":
        return (
          <SignUp onPressCustom={() => this.setState({ switchPage: "LogIn" })} /> 
        );
    }
  }
  render() {
    return <div style={{}}className="customized">{this.switchComponent()}</div>;
  }
}

export default SignInSignUpPattern;
