import React from "react";
import About from "./About/About";
import Home from "./Home/Home";
import Materials from "./Materials/Materials";
import Header from "./Header/Header";
import Footer from "./Footer/Footer";
import LogIn from "./LogIn/LogIn";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "./main.scss";
import Popup from "reactjs-popup";
import SignInSignUpPattern from "./SignInSignUpPattern/SignInSignUpPattern";

function main() {
  return (
    <>
      <Router>
        <div>
          <Header>
            <nav>
              <ul>
                <li>
                  <Link to="/">Home </Link>
                </li>
                <li>
                  <Link to="/about">About</Link>
                </li>
                <li>
                  <Link to="/Materials">Materials</Link>
                </li>
                <div className="user-box">
                  <Popup
                    trigger={<li>Log In</li>}
                    position="center center"
                    modal
                    closeOnDocumentClick
                  >
                     {close => (<SignInSignUpPattern startPage="LogIn" close={close} />)}
                  </Popup>
                  <Popup
                    trigger={<li>Register</li>}
                    position="center center"
                    modal
                    closeOnDocumentClick
                  >
                    <SignInSignUpPattern startPage="SignUp" />
                  </Popup>
                </div>
              </ul>
            </nav>
          </Header>

          {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
          <Switch>
            <Route path="/about">
              <About />
            </Route>
            <Route path="/Materials">
              <Materials />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </div>
      </Router>
      <Footer />
    </>
  );
}

export default main;
