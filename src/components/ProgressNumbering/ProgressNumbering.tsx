import React from "react";
import "./ProgressNumbering.scss";
import DoneIcon from "../../img/icons/done.svg";

class ProgressNumbering extends React.Component<any, any> {
  render() {
    return (
      <>
        <div className="each-progress" onClick={() => this.props.onClick()}>
          <div className="each-progress-index">
            <div className={"each-progress-index-" + this.props.level}>
              {this.props.level === "done" ? (
                <img
                  src={DoneIcon}
                  alt={this.props.index}
                  width="30px"
                  height="30px"
                />
              ) : (
                this.props.index
              )}
            </div>
          </div>
          <div
            className={
              "each-progress-title each-progress-title-" + this.props.level
            }
          >
            {this.props.title}
          </div>
        </div>
      </>
    );
  }
}

export default ProgressNumbering;
