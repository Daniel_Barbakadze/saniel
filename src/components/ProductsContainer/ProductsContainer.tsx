import React from "react";
import "./ProductsContainer.scss";

class ProductsContainer extends React.Component<any, any> {
  render() {
    return this.props.Products.map((prod: any,i: number) => {
      return (
        <div className="products modern-borders" key={i.toString()}>
          <div className={"icon " + prod.ImgClassName}></div>
          <h4>{prod.Title}</h4>
          <h5>{prod.Description}</h5>
        </div>
      );
    });
  }
}
export default ProductsContainer;
