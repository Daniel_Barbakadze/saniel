import React from "react";
import "./footer.scss";
import IconFounders from "../../img/icons/founders.png"
import IconPartners from "../../img/icons/partners.png"
import IconContact from "../../img/icons/contact.png"
import IconPurposes from "../../img/icons/purposes.png"
class Footer extends React.Component {
  render() {
    return (
      <div className="footer">
        <div id="footer-content">
          <div className="footer-section">
            <div className="inside-container">
              <img src={IconFounders} alt="IconFounders"></img>
              <h3>Founders</h3>
              <h5>Sandro Skhirtladze</h5>
              <h5>Daniel Barbakadze</h5>
              <h5>Kristine Tabidze</h5>
            </div>
          </div>
          <div className="footer-section">
            <div className="inside-container">
              <img src={IconPartners} alt="IconPartners"></img>
              <h3>Partners</h3>
              <h5>Zaza Gamezardashvili</h5>
              <h5>Informatics.ge</h5>
              <h5>ABGeo.dev</h5>
            </div>
          </div>
          <div className="footer-section">
            <div className="inside-container">
              <img src={IconContact} alt="IconContact"></img>
              <h3>Contact</h3>
              <h5>+995 591 916 916</h5>
              <h5>Tbilisi, Georgia</h5>
              <h5>support@san.iel</h5>
            </div>
          </div>
          <div className="footer-section">
            <div className="inside-container">
              <img src={IconPurposes} alt="IconPurposes"></img>
              <h3>Future Purposes</h3>
              <h5>Fast Shortcuts</h5>
              <h5>Info about Systems</h5>
              <h5>Tech Support Platform</h5>
            </div>
          </div>
        </div>
        <div className="footer-last-text">
          <h5>All Rights Reserved © 2020</h5>
        </div>
      </div>
    );
  }
}
export default Footer;
