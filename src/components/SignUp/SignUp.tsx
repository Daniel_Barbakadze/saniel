import React from "react";
import "./SignUp.scss";
import ProgressNumbering from "../ProgressNumbering/ProgressNumbering";
import AboutYouProgressBody from "../ProgressBody/AboutYouProgressBody";
import ChooseAnAvatarProgressBody from "../ProgressBody/ChooseAnAvatarProgressBody";
import SetAPasswordProgressBody from "../ProgressBody/SetAPasswordProgressBody";
import InfoIcon from "../../img/icons/info.svg";
import { Form, Button } from "react-bootstrap/";

class SignUp extends React.Component<any, any> {
  state = { active: 1 };
  myRef = React.createRef() as any;

  changeHeadertext(){
    switch(this.state.active){
      case 1:
        return "Who are you?";
      case 2:
        return "Select a picture"
      case 3:
        return "Strong password is recomended"
    }
  }

  transitionAfterRender() {
    this.myRef.current.style.opacity = "0";

    this.myRef.current.style.transition = "opacity .35s ease-in-out";

    setTimeout(() => {
      this.myRef.current.style.opacity = "1";
    }, 1);
  }

  componentDidMount() {
    this.transitionAfterRender();
  }

  renderProgress(index: number) {
    switch (index) {
      case 1:
        return <AboutYouProgressBody />;
      case 2:
        return <ChooseAnAvatarProgressBody />;
      case 3:
        return <SetAPasswordProgressBody />;
    }
  }

  render() {
    return (
      <>
        <div ref={this.myRef} className="popup-page-pattern">
          <div className="pattern-left">
            <div className="pattern-left-header">
              <div className="pattern-left-header-desc">OurName Company.</div>
            </div>
            <div className="pattern-left-progress">
              <ProgressNumbering
                index={1}
                title={"About You"}
                level={"done"}
                onClick={() => this.setState({ active: 1 })}
              />
              <ProgressNumbering
                index={2}
                title={"Choose an Avatar"}
                level={"active"}
                onClick={() => this.setState({ active: 2 })}
              />
              <ProgressNumbering
                index={3}
                title={"Set a password"}
                level={"rounded"}
                onClick={() => this.setState({ active: 3 })}
              />
            </div>
            <div className="pattern-left-footer">
              <div className="pattern-left-footer-icon">
                <img src={InfoIcon} />
              </div>
              <div className="pattern-left-footer-info">
                Please enter real information for our statistics. Your privacy
                is safe.
              </div>
            </div>
          </div>

          <div className="pattern-right">
            <div className="pattern-right-header">
              <h2>{this.changeHeadertext()}</h2>
            </div>
            <div className="pattern-right-progress">
              {this.renderProgress(this.state.active)}
            </div>
            <div className="pattern-right-footer">
              <Form>
                <Form.Group className="formButtons pb-btn-custom">
                  <Button
                    onClick={() =>
                      this.setState({
                        active:
                          this.state.active < 3
                            ? ++this.state.active
                            : this.state.active,
                      })
                    }
                    variant="light"
                    className="pb-btn-custom-finish-later"
                  >
                    Finish Later
                  </Button>
                  <Button
                    variant="primary"
                    type="submit"
                    className="pb-btn-custom-next"
                  >
                    Next
                  </Button>
                </Form.Group>
              </Form>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default SignUp;
