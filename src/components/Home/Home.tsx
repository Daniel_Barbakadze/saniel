import React from "react";
import HomePanel from "./HomePanel";
import ProductsContainer from "../ProductsContainer/ProductsContainer";
import FooterSVG from "../../img/footerSVG.svg";
import HomeService from "./HomeService";
import HomeServiceTests from "./HomeServiceTests";
import HomeServicePosts from "./HomeServicePosts";
import HomeServiceAboutUs from "./HomeServiceAboutUs";
import "./home.scss";

class Home extends React.Component {
  render() {
    return (
      <>
        <HomePanel />

        <div className="home-body">
          <div className="home-body-header">
            <h3>We Provide Awesome Services</h3>
            <div className="home-body-header-divisor"></div>
          </div>

          <div className="home-body-section-products">
            <div className="home-body-section-products-bar">
              <ProductsContainer
                Products={[
                  {
                    ImgClassName: "icon-programming",
                    Title: "Programming",
                    Description:
                      "We have content that you can use on C, C#, C++, Java, Python, Javscript etc.",
                  },
                  {
                    ImgClassName: "icon-algorithms",
                    Title: "Algorithms",
                    Description:
                      "Different algorithms for every programming language you are used to.",
                  },
                  {
                    ImgClassName: "icon-more",
                    Title: "More...",
                    Description:
                      "Tutorials, blogs and other helping stuffs for you. Compilators coming soon.",
                  },
                ]}
              />
            </div>
          </div>

          <HomeService
            HomeServices={[
              {
                HomeServiceName: "HomeServiceTests",
              },
              {
                HomeServiceName: "HomeServicePosts",
              },
              {
                HomeServiceName: "HomeServiceAboutUs",
                Footer: [<img className="footerSVG" src={FooterSVG} alt="FooterSVG"></img>],
              },
            ]}
          />
        </div>
      </>
    );
  }
}
export default Home;
