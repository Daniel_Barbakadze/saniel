import React from "react";

class HomeServicePosts extends React.Component<any, any> {
  render() {
    return (
      <>
        <div className="home-service-2-header">
          <h2>Recent Posts</h2>
          <div className="home-service-2-header-divisor"></div>
        </div>
        <div className="home-service-2-containers">
          <div className="home-service-2-newsfeed-container modern-borders">
            <div className="newsfeed-img"></div>
            <div className="newsfeed-body">
              <h6>Aprils 5, 2019</h6>
              <h3>Why Algorithms</h3>
              <h5>
                In this post you will learn why Algorithms are so important in
                coding.
              </h5>
            </div>
          </div>

          <div className="home-service-2-newsfeed-container modern-borders">
            <div className="newsfeed-img"></div>
            <div className="newsfeed-body">
              <h6>Aprils 4, 2019</h6>
              <h3>Principles of Programming</h3>
              <h5>In this post you will learn principles of programming.</h5>
            </div>
          </div>

          <div className="home-service-2-newsfeed-container modern-borders">
            <div className="newsfeed-img"></div>
            <div className="newsfeed-body">
              <h6>Aprils 1, 2019</h6>
              <h3>Data Structures</h3>
              <h5>In this post you will learn more about Data Structures.</h5>
            </div>
          </div>
        </div>
      </>
    );
  }
}
export default HomeServicePosts;
