import React from "react";
import Cover from "../../img/cover-cropped.png";
import CoverSVG from "../../img/coverSVG1.svg";

class HomePanel extends React.Component<any, any> {
  render() {
    return (
      <div className="home-panel">
        <img className="coverSVG" src={CoverSVG} alt="coverSVG"></img>
        <div className="home-panel-left">
          {/* <p>left</p> */}
          <h1>Free Programming and Algorithms materials</h1>

          <div className="home-panel-left-divisor-line"></div>

          <h4>
            We offer each of you first programming materials, we also have
            materials about different Algorithms. Here you can also write your
            program and test it online. Solve programming problems and get as
            much knoweledge as hard you try.
          </h4>

          <button className="home-panel-left-button">START NOW</button>
        </div>
        <div className="home-panel-right">
          {/* <p>rigth</p> */}
          <img height="500px" src={Cover} alt="cover"></img>
        </div>
      </div>
    )
  }
}
export default HomePanel;
