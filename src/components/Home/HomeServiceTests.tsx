import React from "react";
import ImageTests from "../../img/tests.png";

class HomeServiceTests extends React.Component<any, any> {
  render() {
    return (
      <>
        <div className="home-service-1-container-image">
          <img src={ImageTests} alt="ImageTests"></img>
        </div>
        <div className="home-service-1-container-info">
          <h2>Test Your Skills</h2>
          <h5>
            Choose a subject you want and test your skills using our smart
            advisor. After you've done your test, advisor shows up your result,
            there will be short explanation of correct answer in every mistaken
            answer and also there will be links where you can learn these
            subjects in depths.
          </h5>
          <button className="test-button">TRY IT</button>
        </div>
      </>
    );
  }
}
export default HomeServiceTests;
