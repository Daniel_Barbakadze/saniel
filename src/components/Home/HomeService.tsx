import React, { ReactElement } from "react";
import HomeServiceTests from "./HomeServiceTests";
import HomeServicePosts from "./HomeServicePosts";
import HomeServiceAboutUs from "./HomeServiceAboutUs";

function RenderHS(type: any, components: any) {
  return React.createElement(components[type]);
}

class HomeService extends React.Component<any, any> {
  components = {
    "HomeServiceTests": HomeServiceTests,
    "HomeServicePosts": HomeServicePosts,
    "HomeServiceAboutUs": HomeServiceAboutUs,
  };

  render() {
    return this.props.HomeServices.map((HS: any, i: number) => {
      return (
        <>
        <div className={"home-service-" + (i + 1)} key={i.toString()}>
            <div className={"home-service-" + (i + 1) + "-container"}>
              {RenderHS(HS.HomeServiceName,this.components)}
            </div>
            {HS.Footer && HS.Footer}
        </div>
        </>
      );
    });
  }
}
export default HomeService;
