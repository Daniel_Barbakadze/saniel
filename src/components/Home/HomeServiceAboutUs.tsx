import React from "react";
import ImageAboutUs from "../../img/aboutUs.png";

class HomeServiceAboutUs extends React.Component<any, any> {
  render() {
    return (
      <>
        <div className="home-service-3-aboutus-description">
          <h2>About Us</h2>
          <h5>
            Our project idea aims to explain programming basics as simple
            language as possible to everyone who wants to learn. We also have
            materials for developing Advanced programming skills and subjects
            like Algorithms for better understanding. We also want to include
            more and more technical subjects in a simple way to be accessible
            for people with all proffesions and every age.
          </h5>
        </div>

        <div className="home-service-3-aboutus-img">
          <img src={ImageAboutUs} alt="ImageAboutUs"></img>
        </div>
      </>
    );
  }
}
export default HomeServiceAboutUs;
