import React from "react";
import "./StrengthChecker.scss";

class StrengthChecker extends React.Component<any, any> {
  render() {
    return (
      <>
        <div className="strength-checker">
          <div className="strength-checker-title">{this.props.children}</div>
          <div className="strength-checker-progressbar">
            {Array.apply('', Array(4)).map(() => {
              return <div className="strength-checker-progressbar-peace"></div>;
            })}
          </div>
        </div>
      </>
    );
  }
}

export default StrengthChecker;
