import React from "react";
import { Form } from "react-bootstrap";

class ChooseAnAvatarProgressBody extends React.Component<any, any> {
  fileUpload: any;
  onChangeHandler = (event: any) => {
    console.log(event.target.files[0]);
  };
  render() {
    return (
      <>
        <div className="choose-an-avatar">
          <Form>
            <Form.File
              ref={this.fileUpload}
              id="custom-file"
              type="file"
              name="file"
              onChange={this.onChangeHandler}
              label="upload a photo..."
              custom
            />
          </Form>
          <div>
            {/* Picture Here */}
          </div>
        </div>
      </>
    );
  }
}

export default ChooseAnAvatarProgressBody;
