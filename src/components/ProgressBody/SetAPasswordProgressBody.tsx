import React from "react";
import { Form, InputGroup, FormControl } from "react-bootstrap/";
import "./SetAPasswordProgressBody.scss";
import { ReactComponent as InvisibleIcon } from "../../img/icons/password-invisible.svg";
import { ReactComponent as VisibleIcon } from "../../img/icons/password-visible.svg";
import StrengthChecker from "./StrengthChecker";

class SetAPasswordProgressBody extends React.Component<any, any> {
  state = {
    type: "password",
    typeConfirm: "password",
  };

  handleClick = () =>
    this.setState({
      type: this.state.type === "text" ? "password" : "text",
    });
  handleClickConfirm = () =>
    this.setState({
      typeConfirm: this.state.typeConfirm === "text" ? "password" : "text",
    });

  render() {
    return (
      <>
        <Form className="pb-setapassword">
          {/* <Form.Group controlId="formBasicPassword">
            <Form.Control type={this.state.type} placeholder="Password" />
            <span className="password__show" onClick={this.handleClick}>
              {this.state.type === "text" ? (
                <InvisibleIcon width="25px" height="25px" fill="#ced4da" />
              ) : (
                <VisibleIcon width="25px" height="25px" fill="#ced4da" />
              )}
            </span>
          </Form.Group> */}

          <Form.Group>
            <InputGroup>
              <Form.Control
                style={{
                  borderRight: "unset",
                }}
                type={this.state.type}
                placeholder="Password"
              />
              <InputGroup.Append style={{ height: "35px" }}>
                <InputGroup.Text>
                  <span className="password__show" onClick={this.handleClick}>
                    {this.state.type === "text" ? (
                      <InvisibleIcon
                        width="25px"
                        height="25px"
                        fill="#ced4da"
                      />
                    ) : (
                      <VisibleIcon width="25px" height="25px" fill="#ced4da" />
                    )}
                  </span>
                </InputGroup.Text>
              </InputGroup.Append>
            </InputGroup>
          </Form.Group>

          <Form.Group>
            <InputGroup>
              <Form.Control
                style={{
                  borderRight: "unset",
                }}
                type={this.state.typeConfirm}
                placeholder="Confirm Password"
              />
              <InputGroup.Append style={{ height: "35px" }}>
                <InputGroup.Text>
                  <span className="password__show" onClick={this.handleClickConfirm}>
                    {this.state.typeConfirm === "text" ? (
                      <InvisibleIcon
                        width="25px"
                        height="25px"
                        fill="#ced4da"
                      />
                    ) : (
                      <VisibleIcon width="25px" height="25px" fill="#ced4da" />
                    )}
                  </span>
                </InputGroup.Text>
              </InputGroup.Append>
            </InputGroup>
          </Form.Group>
        </Form>

        <StrengthChecker>
          Strength:
        </StrengthChecker>
      </>
    );
  }
}

export default SetAPasswordProgressBody;
