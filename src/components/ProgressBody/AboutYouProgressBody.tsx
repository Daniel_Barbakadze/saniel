import React from "react";
import {
  Form,
  InputGroup,
  FormControl,
  Row,
  Col,
} from "react-bootstrap/";

import "./AboutYouProgressBody.scss";

class AboutYouProgressBody extends React.Component<any, any> {
  getInitialState() {
    var value = new Date().toISOString();
    return {
      value: value,
    };
  }

  handleChange(value: any, formattedValue: any) {
    this.setState({
      value: value, // ISO String, ex: "2016-11-19T12:00:00.000Z"
      formattedValue: formattedValue, // Formatted String, ex: "11/19/2016"
    });
  }

  componentDidUpdate() {
    // Access ISO String and formatted values from the DOM.
    var hiddenInputElement = document.getElementById("example-datepicker");
    // console.log(hiddenInputElement.value as string); // ISO String, ex: "2016-11-19T12:00:00.000Z"
    // console.log(hiddenInputElement.getAttribute('data-formattedvalue') as any) // Formatted String, ex: "11/19/2016"
  }

  render() {
    return (
      <>
        <Form className="pb-aboutyou">
          <InputGroup className="mb-3">
            <FormControl
              placeholder="Username"
              aria-label="Username"
              aria-describedby="basic-addon1"
            />
          </InputGroup>

          <Form.Group>
            <Row>
              <Col>
                <Form.Control placeholder="First name" />
              </Col>
              <Col>
                <Form.Control placeholder="Last name" />
              </Col>
            </Row>
          </Form.Group>

          <Form.Group controlId="formBasicEmail">
            <Form.Control type="email" placeholder="Email" />
          </Form.Group>

          <Form.Group>
            <Row>
              <Col>
                <Form.Control
                  type="date"
                  data-date-format="DD MMMM YYYY"
                  placeholder="Enter email"
                />
              </Col>
              <Col>
            <Form.Control type="text" placeholder="Phone Number" />
              </Col>
            </Row>
          </Form.Group>
        </Form>
      </>
    );
  }
}

export default AboutYouProgressBody;
