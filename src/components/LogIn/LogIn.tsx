import React from "react";
import "./LogIn.scss";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import SignInSignUpPattern from "../SignInSignUpPattern/SignInSignUpPattern";

class LogIn extends React.Component<any, any> {

  myRef = React.createRef() as any;

  fadeout(){
    this.myRef.current.style.opacity = "0";
  }

  transitionAfterRender(){
    this.myRef.current.style.opacity = "0";

    this.myRef.current.style.transition = "opacity .35s ease-in-out";

    setTimeout(() => {
      this.myRef.current.style.opacity = "1";
    }, 1);
  }

  componentDidMount() {
    this.transitionAfterRender();
  }

  render() {
    return (
      <div className="LogInWrapper" ref={this.myRef}>
        <div className="form-header">
          <p className="text-center">Log In</p>
        </div>
        <Form>
          <Form.Group controlId="formBasicEmail">
            <Form.Control type="email" placeholder="Enter email" />
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Control type="password" placeholder="Password" />
          </Form.Group>
          <Form.Group controlId="formBasicCheckbox">
            <Form.Text
              className="text-right form-hyperlink"
              onClick={() => this.props.onPressCustom()}
            >
              Forgot Password?
            </Form.Text>
          </Form.Group>
          <Form.Group className="formButtons">
            <Button variant="primary" type="submit">
              Log in
            </Button>
            <Button
              onClick={() => this.props.close()}
              variant="light"
              style={{ background: "transparent" }}
            >
              Cancel
            </Button>
          </Form.Group>
        </Form>
        <div className="form-footer">
          <p className="text-center">
            Don't have an account?
            <span
              onClick={() => this.props.onPressCustom2()}
              className="form-hyperlink"
            >
              {" "}
              Sign up
            </span>
          </p>
        </div>
      </div>
    );
  }
}
export default LogIn;
